### What is this repository for? ###

This is a program for solving the statistical On-Off problem. 

This program calculates the Bayes factor("B_01"), the Bayesian z-value("S_B_01"), PDF("P_lambda"), CDF("C_lambda"), 
together with interesting points on the PDF, such as 99% credible UL ("l_99")
and the 16% and 84% credible UL for calculating the central interval
containing 68% signal posterior probability (for signal error estimation)

## All methods based on: ##
	
* Max L. Knoetig: Signal Discovery, Limits, and Uncertainties with Sparse On/Off Measurements: An Objective Bayesian Analysis; The Astrophysical Journal, 790, 2, 106; 2014 http://stacks.iop.org/0004-637X/790/i=2/a=106

* Max L. Ahnen: On the On-Off Problem: An Objective Bayesian Analysis; InProceedings, ICRC 2015, The Hague

## A note: ##
This method was checked roughly up to non=120, noff=200 @ alpha = 0.1 
You need to calculate higher numbers together with smaller alpha?
use Mathematica -> thousands of counts possible, using 
higher precision calculations on the special functions, see:
http://community.wolfram.com/groups/-/m/t/102242?p_p_auth=K8r5Y8QR

### How do I get set up? ###

* checkout the repo
* Dependencies: numpy, scipy, mpmath (included, for instance, in sympy)